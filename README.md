# 7.PCSE_TP_Bluetooth

Trabajo práctico de protocolo y módulo Blutooth, de la materia Protocolos de Comunicación en Sistemas Embebidos.


### Resumen
Hice modificaciones al ejemplo "hm10_uart_bridge", eliminando los comandos "LED_ON" y "LED_OFF" que encendían el led en la pantalla del celular, y agregando el envío de coordenadas gps apretando la tecla TEC4

En el lado de la app del celular, cuando se reciben las coordenadas, se crea un marcador sobre el mapa en las coordenadas recibidas y se hace zoom sobre esa área.

Del ejemplo visto en clase se mantuvo el encendido del led de la EDU-CIAA desde la app, y la impresión de los comandos AT.